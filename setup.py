from distutils.core import setup
from distutils.errors import DistutilsError
from distutils.command import config, build, sdist

from sys import version, hexversion

class Sdist(sdist.sdist):
    default_format = {'posix': 'bztar'}
        
class Config(config.config):
    def run(self):
        if hexversion < 0x20400f0:
            raise DistutilsError(
                'Python interpreter is too old: python >= 2.4.0 ' +
                'needed, %s detected' % version[:5])
        if hexversion < 0x20500f0:
            try:
                __import__('elementtree.ElementTree')
            except ImportError:
                raise DistutilsError(
                'ElementTree module needed on Python 2.4.x: install it first')
        
class Build(build.build):
    def run(self):
        Config(self.distribution).run()
        build.build.run(self)

setup(name='radcan',
      version='0.9.4',
      license='GPL',
      platforms='posix',
      description='Radio-Canada Video-on-Demand web service client',
      url='http://radcan.googlepages.com/index.html',
      author='Sylvain Fourmanoit',
      author_email='syfou@users.sourceforge.net',
      scripts=['radcan'],
      py_modules=['radcan'],
      cmdclass = {
      'config' : Config,
      'build' : Build,
      'sdist' : Sdist
      }
    )
