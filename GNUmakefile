# Just a quick makefile to help keeping the whole thing coherent and 
# up to date (documentation, internal versionning).
#
# It needs:
#
# - GNU Make (obviously!)
# - GNU sed
# - Git and Cogito for tagging
# - POSIX compliant date util (tested on GNU coreutils)
# - help2man, to regenerate the man page
# - asciidoc and htmltidy, to refresh the html doc
# - links, to generate the text-only README
#
DOC=radcan.1 README index.html
DATE=$(shell env LC_ALL=fr_FR date +'%A, le %d %B %Y')
VER=$(shell cg tag-ls | sed '$$!d;s/\([0-9\.]*\).*/\1/')
OLD=$(shell cat .version 2> /dev/null)

all: tag $(DOC)

radcan.1: radcan radcan.py radcan.inc
	help2man --include radcan.inc --no-info ./$< > $@

README: index.html
	links -html-numbered-links 1 -dump $< > $@

index.html: readme.txt
	asciidoc -a revision="$(VER)" -a localdate="$(DATE)" \
                --unsafe \
		-a stylesdir=/etc/asciidoc/stylesheets \
		-f asciidoc.conf -o $@ $<
	-tidy  --input-encoding utf8 --output-encoding ascii \
		-m -i $@

.PHONY: clean tag

clean:
	-rm -f *.pyc *~ 
	-rm -f $(DOC)

# Versionning support: call with `make VER=xxx' to force upgrade.
#
.version:
	@touch .version

setup.py radcan.py index.html: .version

tag: .version
ifneq ($(VER),$(OLD))
	sed -i "s/^\(__version__ = \).*/\1 '$(VER)'/" radcan.py
	sed -i "s/\(version=\).*/\1'$(VER)',/" setup.py
	@echo $(VER) > .version
endif
