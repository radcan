Client pour le service web de vidéo sur demande de Radio-Canada
===============================================================
Sylvain Fourmanoit <syfou@users.sourceforge.net>

///////////////////////////////////////////////////////////////////////
Ceci est un document asciidoc, encodé en UTF-8.

http://www.methods.co.nz/asciidoc/

Voir GNUMakefile, dans l'entrepôt, pour la façon dont est régénérée 
la version formatée.
///////////////////////////////////////////////////////////////////////

***********************************************************************
La dernière version de ce document est
http://radcan.googlepages.com/index.html[disponible ici] en tout temps.
***********************************************************************

[NOTE]
========================================================================
Fin décembre 2007, la Société Radio-Canada a à nouveau modifié
la structure de sa diffusion en mode continu ("'streaming'"), dont la
dernière modification importante, aux conséquences funestes pour ce
code, datait d'octobre 2007.

La nouvelle méthode de diffusion rend les flots utilisables par des
décodeurs audio-vidéo en
http://fr.wikipedia.org/wiki/Code_source_libre[code ouvert], et *ce
client permet donc à nouveau, en combinaison avec ceux-ci, d'aller les
consulter*.
========================================================================

Qu'est-ce que ceci?
-------------------
Un client permettant l'accès au contenu de la
http://www.radio-canada.ca/audio-video/[zone audio vidéo de la Société
Radio-Canada] de façon
http://fr.wikipedia.org/wiki/Code_source_libre[ouverte] et
http://fr.wikipedia.org/wiki/Portabilite_%28informatique%29[portable].

Pré-requis
---------
Soit:

- http://python.org/[Python] 2.5.0, ou toute version plus récente: aucun module
  supplémentaire nécéssaire.
- Ou encore Python 2.4.x, accompagné du module
  http://effbot.org/zone/element-index.htm[ElementTree].

Installation
------------
- http://radcan.googlepages.com/radcan-{revision}.tar.bz2[Télécharger] le
  dernier paquetage, ou la version de
  http://repo.or.cz/w/radcan.git[l'entrepôt] (méthode recommandée).

- Utiliser le client tel quel, en invoquant `./radcan`, ou l'intégrer dans
  votre environnement Python, grâce au script `setup.py` fourni:

-----------------------
python setup.py install
-----------------------

Dans ce dernier cas, l'invocation en ligne de commande de `radcan` deviendra
généralement accessible à tous les utilisateurs du système.

Utilisation
-----------
Voir `radcan \--help`, ou le manuel (`radcan.1`) pour le détail. Voici
quelques exemples d'utilisation:

- Obtenir la liste de toutes les émissions disponibles: `radcan
  \--broadcasts`.

- Obtenir tous les clips des quinze derniers jours traitant de politique:
  `radcan \--search=politique \--date-offset=15`.

- Obtenir uniquement les liens au premier et au troisième clip de la liste
  des clips suggérés aujourd'hui: `radcan \--suggested \--asx
  \--items=0,2`.

- Obtenir tous les télé-journaux nationaux (émission #1656) récents: `radcan
  \--query=1656`.

Questions et remarques fréquentes
---------------------------------

La zone audio vidéo de Radio-Canada marche très bien: quel est l'intérêt?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
- Ne pas avoir à transférer 400 KB à chaque fois qu'on réactualise la page
  principale de la zone, et 200 KB pour la console: mais seulement environ
  30 KB pour une requête typique... C'est d'autant plus pratique que
  souvent, les requêtes échouent ou n'aboutissent pas, surtout en heure de
  grande affluence.

- Interroger Radio-canada sur son contenu audio-vidéo significativement
  plus rapidement, et avec une demande en ressources nettement plus faible
  que par le site: ce client tourne correctement sur un Pentium original
  avec 32 MB de mémoire vive.
 
- Interroger Radio-canada quant à son contenu audio-vidéo sur toute
  plate-forme supportant Python, ce qui rejoint beaucoup plus d'utilisateurs
  que Flash.

- Ne pas dépendre de http://fr.wikipedia.org/wiki/Adobe_Flash[Flash], une
  composante logicielle fermée et propriétaire, pour avoir accès à ce
  service de Radio-Canada.

- Avoir la possibilité de scripter l'accès au contenu Radio-Canadien: le
  téléchargement d'un clip audio donné sur une tâche périodique (cronjob)
  devient possible: c'est l'équivalent d'avoir la balladodiffusion générale
  pour tout Radio-Canada!

Ce client ne renvoie que des liens aux clips: comment suis-je sensé les lire?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Ce client renvoie des liens à des fichiers
http://fr.wikipedia.org/wiki/ASX_%28format%29[ASX], qui "pointent" à une ou
plusieurs sources http://fr.wikipedia.org/wiki/Windows_Media_Video[WMV],
toutes en WMV2 ou WMV3, un codec propriétaire. Vous serez cependant heureux
d'apprendre que depuis environ un an, la librairie
http://ffmpeg.mplayerhq.hu/[FFMPEG] contient un décodeur entièrement libre
(module VC-1) qui, sans encore supporter correctement l'ensemble des
variantes de cette famille (VC-1, WMV2, WMV3, WMV9, ...), restitue déjà très
correctement l'ensemble des flots de Radio-Canada sur un ensemble large
d'architectures et de systèmes d'exploitation: minimalement, l'ensemble des
lecteurs vidéo utilisant FFMPEG (tels http://www.mplayerhq.hu/[mplayer] ou
http://www.videolan.org/vlc/[VLC Media Player], pour ne nommer que ceux-là)
seront donc capable de lire les clips. Par exemple, un utilisateur de
mplayer pourra utiliser:

----------------------------------------------------------
mplayer -playlist http://chemin/au/fichier_redirecteur.asx
----------------------------------------------------------

Ce client ne semble pas retourner les mêmes résultats que le site.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Au meilleur de notre connaissance, ce client renvoie exactement la même
information que celle disponible par la zone audio vidéo de Radio-Canada.
Gardez à l'esprit que certaines requêtes, tel celles permettant d'avoir les
clips suggérés, voient leurs résultats changer très souvent: il est donc
tout à fait possible d'obtenir une sortie différente pour la même requête,
parfois à quelques secondes d'intervalle.

Les résultats ne sont pas constants!
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
C'est vrai. Voir l'entrée précédente.

Certaines requêtes ne retournent rien.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Quand une requête ne renvoie rien sans non plus signaler la moindre erreur,
cela signifie généralement qu'il n'existe aucune résultat qui corresponde à
la requête: par exemple, demander les clips récents d'une émission qui fait
relâche, ou qui n'est pas encore mise en ligne dans le nouveau système ne
retournera rien, sans non plus générer d'erreur.

Il arrive également que Radio-Canada signale à l'occasion qu'il n'existe
aucun résultat correspondant à une requête qui devrait être valide: au
meilleur de notre compréhension, il s'agit d'une méthode de contrôle de la
congestion quant trop de requêtes se télescopent. Soumettre la même
requête une seconde fois permet alors habituellement d'obtenir le résultat
escompté.

Pourquoi l'interface est-elle en anglais?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Par souci de portabilité et d'uniformité: sur de nombreux environnements Python,
l'aide automatisée emploierait un horrible sabir franco-anglais. La gestion des
encodages de caractères n'est pas non plus très uniforme d'une plate-forme à
l'autre, et nous désirions éviter les problèmes.

Pour être parfaitement honnête, le problème est aussi esthétique: un code
bilingue est souvent plus équivoque et difficile à suivre (changement
continuel de terminologie, etc.).

Je suis intrigué: comment ça marche?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Allez lire le source. ;-D

Vous privez Radio-Canada de précieux revenus publicitaires!
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
C'est faux. Au moment d'écrire ces lignes, le site lui-même ne diffuse
aucune publicité directement: certains clips vidéo contiennent des
publicités, et ce client n'y change absolument rien.

En fait, sur une base strictement économique, l'utilisation de ce client
par un usager devrait être marginalement plus rentable pour Radio-Canada
que l'utilisation du site, puisqu'il diminue significativement le besoin en
bande passante lors de la phase de recherche tout en ne modifiant pas la
quantité de publicité à laquelle ceux-ci seront exposés lors de leur
écoute.
