"""
Client to Radio-Canada's Video-on-Demand web service. It can be reused
as a module, or invoked as a command line script.
"""
#-------------------------------------------------------------------------------
# Legalese
#
__copyright__ = \
'''Copyright(C), 2007, 2008, Sylvain Fourmanoit <syfou@users.sourceforge.net>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to
deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies of the Software and its documentation and acknowledgment shall be
given in the documentation and software packages that this Software was
used.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.   
'''
__version__ =  '0.9.4'

#-------------------------------------------------------------------------------
#
import httplib, sys, os, optparse, pprint

try:
    import xml.etree.ElementTree as etree
except ImportError:
    import elementtree.ElementTree as etree

import xml.parsers.expat

#-------------------------------------------------------------------------------
# Utility class
class Missing(dict):
    """__missing__ keyword emulating container for python 2.4"""
    def __init__(self, iterable=None, default=''):
        self.default = default
        if iterable: dict.__init__(self, iterable)
    def __getitem__(self, k):
        try:
            return dict.__getitem__(self, k)
        except KeyError:
            try:
                return self.default % k
            except TypeError:
                return self.default

#-------------------------------------------------------------------------------
# XML answer processing
#
class XML2Py:
    """
    Convert an arbitrary XML file to a pythonic structure.

    Right now, it is instanciated from the Console.SOAPQuery instances
    during calls to console: this could be modularised (we could
    basically just return a file-like object, and let the caller deals
    with the xml content) but we didn't see a real need: set
    Console.debug to true if ever you need to debug.

    Be aware that dynamically changing XML2Py will likely break
    Console.GetList2Alt().
    """
    class TreeBuilder(etree.XMLTreeBuilder):
        """Get rid of all the name space cruft"""
        def _fixname(self, key): return key.split('}')[-1]
        
    def __call__(self, f):
        return self._myiter(
            etree.parse(f, self.TreeBuilder()).getroot())
    
    def _myiter(self, root):
        def cond(subtree, text):
            try:
                if len(subtree)>0:
                    return subtree
                elif text is None:
                    return None
                elif text.isdigit():
                    return int(text)
                elif text.lower() == 'true':
                    return True
                elif text.lower() == 'false':
                    return False
            except: pass
            return text
        tree = {}
        for e in root:
            subtree = self._myiter(e)
            if e.tag in tree:
                if type(tree[e.tag]) is list:
                    tree[e.tag].append(cond(subtree, e.text))
                else:
                    tree[e.tag]= [tree[e.tag], cond(subtree, e.text)]
            else:
                tree[e.tag] = cond(subtree, e.text)
        if len(tree) == 1:
            tree = tree.values()[0]
        return tree

#-------------------------------------------------------------------------------
# SOAP interface access
#
class Console:
    """
    Radio-Canada querying engine. Just use something like:

    console.GetLists2(MotsCle='politique')

    and you are done!
    """
    queries = {
        'GetEmissions': {
            'fields': {'IDRegion': 1}
            },
        'GetListSuggere': {
            'fields': {'IdMedia': -1,
                       'IdRegion': -1}
            },
        'GetListAujourdhui': {
            'fields': {'IdRegion': -1}
            },
        'GetList2': {
            'fields': {'IDEmission': 'del',
                       'Chaine': None,
                       'Video': -1,
                       'IDTypeContenu': 0,
                       'MotsCle': None,
                       'StartItem': 1,
                       'NbrItem': 10,
                       'DateOffset': 7,
                       'NbJours': 0,
                       'Tri': 'date:D',
                       'StrIDTypeContenu': None,
                       'StrIDCategorie': None,
                       'StrIDGenre': None,
                       'IDReseau': 0,
                       'IDRegion': -1,
                       'Integrale': -1},
            'prebody': '<requete>',
            'postbody':'</requete>'}
        }
    
    class SOAPQuery:
        """
        Query a given port of Radio-Canada SOAP interface.
        
        Returned by Console.__getattr__().
        """
        request = '''<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xmlns:xsd="http://www.w3.org/2001/XMLSchema"
  xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <%(port)s xmlns="http://tempuri.org/">
%(prebody)s
%(body)s
%(postbody)s
    </%(port)s>
  </soap:Body>
</soap:Envelope>'''
        class Attrs(dict):
            def __missing__(self, name): return ''
        class Tee:
            '''Automated echo of file-like objects on read'''
            def __init__(self, f, stream=None, tee=True):
                self.f = f
                if stream is not None:
                    self.stream = stream
                else:
                    self.stream = sys.stdout
                self.tee = tee
        
            def read(self, size=-1):
                s = self.f.read(size)
                if self.tee:
                    self.stream.write(s)
                return s
        
        def __init__(self, **kw):
            self.kw = Missing(kw)
            
        def splitproxy(self, http_proxy):
            if 'http://' == http_proxy[:7]:
                return http_proxy[7:].split(':')
            else:
                raise RuntimeError('http proxy is not in the expected ' +
                                   'http://host:port format')
            
        def __call__(self, **fields):
            def tag(k, v):
                if v is not None:
                    return '<%s>%s</%s>' % (k, v, k)
                else:
                    return '<%s/>' % k
                
            # Prepare the request
            #
            self.kw['body'] = '\n'.join([tag(k, v) for k, v in
                                         dict(self.kw['fields'].items() +
                                              fields.items()).items()
                                         if v != 'del'])
            request = self.request % self.kw
            headers = {'Content-Type': 'text/xml',
                       'SOAPAction': 'http://tempuri.org/%(port)s' % self.kw }

            # Set up the connection parameters, either directly or through
            # a proxy
            if self.kw['proxy'] is not None:
                host, port = self.splitproxy(self.kw['proxy'])
                uri = 'http://www1.radio-canada.ca/aspx/WSConsole/console.asmx'
            else:
                host, port = ('www1.radio-canada.ca', 80)
                uri = '/aspx/WSConsole/console.asmx'
                
            # Finally, time to connect
            for i in range(self.kw['retry']):
                conn = httplib.HTTPConnection(host, port)
                conn.set_debuglevel((0,2)[self.kw['debug'] is True])
                conn.connect()
                conn.request('POST', uri, request, headers)
                try:
                    ret = XML2Py()(self.Tee(conn.getresponse(), tee=self.kw['debug']))
                    break
                except xml.parsers.expat.ExpatError:
                    ret = {}
            return ret

    def __init__(self, debug=False, proxy=None, retry=3):
        """
        Initialize the console, setting the debug flag, http proxy, 
        and number of retries in case of malformed answer.

        proxy is expected to be a string, of the usual http://host:port
        format, following the unix-style http_proxy environment variable.
        """
        self.debug = debug
        self.proxy = proxy
        self.retry = retry
    
    def __getattr__(self, port):
        """
        Call a SOAP port, as defined in Radio-Canada's spec:

        http://www1.radio-canada.ca/aspx/WSConsole/console.asmx?WSDL
        """
        # We do support calls to arbitrary ports, but we also
        # includes out-of-the-box values for a couple of them:
        # see self.queries.
        if port[:2] == '__':
            raise AttributeError('no attribute %s' % port)
        else:
            return self.SOAPQuery(port = port, debug = self.debug,
                                  proxy = self.proxy, retry = self.retry,
                                  **self.queries.get(port,{'fields':{}}))

    def GetList2Alt(self, **kw):
        """
        Special wrapper to GetList2 port: for the sake of uniformity,
        it is significantly easier to deal with some medias iterable
        (as with other ports such as GetListAujourdhui or
        GetListSuggere): this is what this method supplies, by calling
        the port by chunks of ten items, then spoonfeeding the result
        to the caller.
        """ 
        for k in ('NbrItem', 'StartItem'):
            if k in kw:
                del kw[k]
                
        r = self.GetList2(NbrItem=1, **kw)
        if 'Erreur' in r and r['Erreur'] is not None:
            print >> sys.stderr, 'server error:', r['Erreur']
        if 'NbResultats' not in r:
            return
        for i in range(1, r['NbResultats'], 10):
            r = self.GetList2(StartItem=i, NbrItem=10, **kw)
            for media in r['Medias']:
                yield media

#-------------------------------------------------------------------------------
# Generic formatting
#
# Here is a couple of templates used by cli()...
#
templates = {'verboseMedia' : '''CLip %(Pos)d (Media ID %(IDMedia)s)
Diffusion: %(HeureDiffusionStr)s %(DateDiffusionStr)s
Duration: %(Duree)s seconds
Broadcast: %(NomEmission)s (Broadcast ID %(IDEmission)s)
Description: [%(ExtraitTitre)s]
Network: %(NomReseau)s
Integral: %(IsIntegral)s
Video: %(Video)s
URI: %(LienASX)s
''' + '='*80,
             'linkMedia': '%(LienASX)s',
             'broadcasts': '%(Nom)s (ID %(IDEmission)d)'
             }

# ... And there is the templating "engine"
#
def apply_template(iterable, template=None, missing = '<%s: N/A>',
                   items=None, encoding='utf-8', stream=None):
    """
    Output some query results obtained from Console on file-like
    stream (or sys.stdout if none given), after formatting them; the
    items yield by the iterable are expected to support being mapped
    on a dictionnary.

    See cli() for example on how it can be used.
    """
    if stream is None: stream = sys.stdout
        
    for i, item in enumerate(iterable):
        if items is None or i in items:
            if template is None:
                print >> stream, ('--- Clip %d ' % i) +  '-'*60
                pprint.pprint(item, stream=stream)
            else:
                item['Pos'] = i
                print >>stream, (template %
                                 Missing(item, missing)).encode(encoding)
            
#-------------------------------------------------------------------------------
# Now, specify a simple CLI interface
#
def cli():
    """
    Simple CLI interface covering the most useful/common cases (looks
    at sys.argv to decide what to do).
    """
    
    # Generate and parse the command line
    #
    p = optparse.OptionParser(
        usage="""%prog [options]
    
Radio-Canada's Video on demand non-interactive command line interface.""",
        version='''%%prog %s
Copyright (C) 2007, 2008 S.Fourmanoit <syfou@users.sourceforge.net>.
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
'''.strip() % __version__)
    p.add_option('-b', '--broadcasts',
                 dest='list_broadcasts', action="store_true", default=False,
                 help='list all available broadcasts')
    p.add_option('-s', '--suggested',
                 dest='list_suggested', action="store_true", default=False,
                 help='list suggested clips')
    p.add_option('-t', '--today',
                 dest='list_today', action='store_true', default=False,
                 help='list today\'s clips')
    p.add_option('-q', '--query',
                 dest='broadcast_id', default=None,
                 help='''query clips for a given broadcast (use --broadcasts
                         for a list of all possible broadcast IDs)''')
    p.add_option('-k', '--search',
                 dest='keywords', default=None,
                 help='''query clips based on keyword search''')
    p.add_option('-d', '--date-offset',
                 dest='days', default=7,
                 help='''specify how far in time to look back (default is 7,
                         i.e. looking one week back): this only applies to
                         --query and --search requests''')
    p.add_option('-i', '--items',
                 dest='items', default=None,
                 help='''limit output to a list of comma-separated items,
                         based on their indexed position in the server
                         response (for instance, specifying "0, 2" will
                         make the script printout only the templated output
                         for the first and third item''')
    p.add_option('--template',
                 dest='template', default=None,
                 help='''manually specify output template to replace default
                        (advanced usage: read the code if ever you need
                        this)''')
    p.add_option('--encoding',
                 dest='encoding', default='utf-8',
                 help='specify output console encoding (default: utf-8)')
    p.add_option('-r', '--raw',
                 dest='raw', action="store_true", default=False,
                 help='''Force brute, detailed output (ignore any template):
                         work for all requests''')
    p.add_option('-a', '--asx',
                 dest='asx', action="store_true", default=False,
                 help='''Force output of ASX links only (ignore any template):
                         make sense for all but --broadcasts requests''')
    p.add_option('-p', '--proxy',
                 dest='http_proxy', default=None,
                 help='''specify what http proxy to use, as a string of
                         the form "http://host:port". By default,
                         the content of environment variable http_proxy
                         is used, if set. If neither this option nor
                         the http_proxy variable is specified, the script
                         connects to Radio-Canada directly''')
    p.add_option('--retry',
                 dest='retry', default=3, type="int", 
                 help='''specify how many times to resend a request in case of 
                         a malformed XML answer. Default is 3 times.''')
    p.add_option('--debug',
                 dest='debug', action="store_true", default=False,
                 help='''Send complete trace of client<->server transactions
                         on stdout (data sent and data received,
                         including headers)''')
                 
    opts, args = p.parse_args()

    # Initialize the console
    #
    if opts.http_proxy is None: opts.http_proxy = os.getenv('http_proxy')
    console = Console(debug=opts.debug, proxy=opts.http_proxy, retry=opts.retry)
    
    # Set the various parameters based on mode
    #
    # Default keywords and templates...
    kw = {}
    template = templates['verboseMedia']

    # Then, make adjustments based on invokation
    #
    if opts.list_broadcasts:
        template = templates['broadcasts']
        port = console.GetEmissions
    elif opts.list_suggested:
        port = console.GetListSuggere
    elif opts.list_today:
        port = console.GetListAujourdhui
    elif opts.broadcast_id is not None or opts.keywords is not None:
        if opts.broadcast_id is not None: kw['IDEmission'] = opts.broadcast_id
        if opts.keywords is not None: kw['MotsCle'] = opts.keywords
        kw['DateOffset'] = opts.days
        port = console.GetList2Alt
    else:
        p.error(' '.join(
            ['no request specified (one of --broadcasts, --suggested,',
             '--today, --query or --search), bailing out. See --help',
             'for details.']))
        
    # Make last minutes adjustments to the template
    #
    if opts.template is not None: template = opts.template
    if opts.raw: template=None
    if opts.asx: template=templates['linkMedia']

    # And check for items output limitations
    #
    if opts.items is not None:
        opts.items = [int(i) for i in opts.items.split(',')]
        
    # Fire in the hole!
    #
    try:
        apply_template(port(**kw), items=opts.items,
                       template=template, encoding=opts.encoding)
    except RuntimeError, e:
        p.error(str(e))
    except:
        print >> sys.stderr, 'An error occured while processing the request:'
        raise

#-------------------------------------------------------------------------------
#
if __name__ == '__main__':
    cli()
